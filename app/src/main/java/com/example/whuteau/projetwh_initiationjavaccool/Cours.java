package com.example.whuteau.projetwh_initiationjavaccool;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Date;

/**
 * Created by whuteau on 13/11/2017.
 */
public abstract class Cours {

        //Parametres

    protected String intituleCours;
    protected Date dateCours;
    protected String heureDebutCours;
    protected int nbMaxCours;
    protected ArrayList<Participant>lesParticipantsInscrits;

        //Accesseurs
    public String getIntituleCours() {
        return intituleCours;
    }


    public Date getDateCours() {
        return dateCours;
    }

    public void setDateCours(Date dateCours) {
        this.dateCours = dateCours;
    }


    public String getHeureDebutCours() {
        return heureDebutCours;
    }

    public void setHeureDebutCours(String heureDebutCours) {
        this.heureDebutCours = heureDebutCours;
    }


    public int getNbMaxCours() {
        return nbMaxCours;
    }

    public void setNbMaxCours(int nbMaxCours) {
        this.nbMaxCours = nbMaxCours;
    }


    public ArrayList<Participant> getLesParticipantsInscrits() {
        return lesParticipantsInscrits;
    }


        //Constructeur
    public Cours(String intitule, Date date, String heure){
        intituleCours = intitule;
        dateCours = date;
        heureDebutCours = heure;
        lesParticipantsInscrits = new ArrayList<Participant>();
    }

        //Methodes
    public abstract boolean ajouterParticipant(Participant unP);

    public abstract void supprimerParticipant(Participant unP) throws Exception;

    public abstract String listeDesParticipants();

    public abstract ArrayList<Participant> TrouveParticipants(String unNomFamille);

    @Override
    public abstract String toString();

}

