package com.example.whuteau.projetwh_initiationjavaccool;

import junit.framework.TestCase;

import java.text.SimpleDateFormat;
import java.util.ArrayList;

/**
 * Created by whuteau on 04/12/2017.
 */
public class SaisonTest extends TestCase {

    public SaisonTest(String testMethodName) {
        super(testMethodName);
    }

    public void testAjoutCours() throws Exception {
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
        Saison s = new Saison("Hiver2017");
        Cours c = new CoursCollectifs("Ski", sdf.parse("04/12/2017"), "14 heures", 5);
        Cours c1 = new CoursCollectifs("Randonnée", sdf.parse("08/12/2017"), "9 heures", 8);
        Cours c2 = new CoursCollectifs("Ski", sdf.parse("09/12/2017"), "13 heures", 4);

        s.ajoutCours(c);
        s.ajoutCours(c1);
        s.ajoutCours(c2);

        assertEquals("Cours non ajouté !", s.getLesCours().size(), 3);
    }

    public void testSupprimerCours() throws Exception {
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
        Saison s = new Saison("Hiver2017");
        Cours c = new CoursCollectifs("Ski", sdf.parse("04/12/2017"), "14 heures", 5);
        Cours c1 = new CoursCollectifs("Randonnée", sdf.parse("08/12/2017"), "9 heures", 8);
        Cours c2 = new CoursCollectifs("Ski", sdf.parse("09/12/2017"), "13 heures", 4);

        s.ajoutCours(c);
        s.ajoutCours(c1);
        s.ajoutCours(c2);
        assertEquals("Cours non ajouté !", s.getLesCours().size(), 3);
        s.supprimerCours(c);
        assertEquals("Cours non ajouté !", s.getLesCours().size(), 2);
        s.supprimerCours(c1);
        assertEquals("Cours non ajouté !", s.getLesCours().size(), 1);
        s.supprimerCours(c2);
        assertEquals("Cours non ajouté !", s.getLesCours().size(), 0);
    }

    public void testTrouveCours() throws Exception {
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
        Saison s = new Saison("Hiver2017");
        Cours c = new CoursCollectifs("Ski", sdf.parse("04/12/2017"), "14 heures", 5);
        Cours c1 = new CoursCollectifs("Randonnee", sdf.parse("08/12/2017"), "9 heures", 8);
        Cours c2 = new CoursCollectifs("Ski", sdf.parse("09/12/2017"), "13 heures", 4);

        s.ajoutCours(c);
        s.ajoutCours(c1);
        s.ajoutCours(c2);

        ArrayList<Cours> TrouverCours = new ArrayList<Cours>();

        //1 participant
        TrouverCours = s.TrouveCoursI("Randonnee");
        assertEquals("Cours non trouvé", TrouverCours.size(), 1);

        //2 participants
        TrouverCours = s.TrouveCoursI("Ski");
        assertEquals("Cours non trouvé", TrouverCours.size(), 2);

        //1 participant
        TrouverCours = s.TrouveCoursD(sdf.parse("04/12/2017"));
        assertEquals("Cours non trouvé", TrouverCours.size(), 1);
    }
}
