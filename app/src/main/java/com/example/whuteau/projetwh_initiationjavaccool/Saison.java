package com.example.whuteau.projetwh_initiationjavaccool;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

/**
 * Created by whuteau on 04/12/2017.
 */
public class Saison {

        //propriétés

    private String saison;
    private ArrayList<Cours> lesCours;
    private Date dateDebut;
    private Date dateFin;

        //constructeur

    public Saison(String saison, Date dateDebut, Date dateFin) {
        this.saison = saison;
        this.dateDebut = dateDebut;
        this.dateFin = dateFin;
        lesCours = new ArrayList<>();
    }

        //accesseurs

    public String getSaison() {
        return saison;
    }

    public void setSaison(String saison) {
        this.saison = saison;
    }

    public ArrayList<Cours> getLesCours() {
        return lesCours;
    }

    public Date getDateDebut() {
        return dateDebut;
    }

    public void setDateDebut(Date dateDebut) {
        this.dateDebut = dateDebut;
    }

    public Date getDateFin() {
        return dateFin;
    }

    public void setDateFin(Date dateFin) {
        this.dateFin = dateFin;
    }

    public void setLesCours(ArrayList<Cours> lesCours) {
        this.lesCours = lesCours;
    }


    public void ajoutCours(Cours unC){
            lesCours.add(unC);
    }

    public void supprimerCours(Cours unC) throws Exception{
        boolean CoursPresent = false;
        CoursPresent = lesCours.remove(unC);
        if(CoursPresent == false){
            throw new Exception("Participant non supprimé");
        }
    }

    public String listeDesCours(){
        StringBuilder liste = new StringBuilder();
        for(Cours C : lesCours){
            liste.append(C.toString()).append("\n");
        }
        return liste.toString();
    }

    public ArrayList<Cours> TrouveCoursI(String intituleCours){
        ArrayList<Cours>lesCoursTrouves = new ArrayList<Cours>();
        for (Cours C : lesCours){
            if(C.getIntituleCours().equals(intituleCours)){
                lesCoursTrouves.add(C);
            }
        }
        return lesCoursTrouves;
    }

    public ArrayList<Cours> TrouveCoursD(Date dateCours){
        ArrayList<Cours>lesCoursTrouves = new ArrayList<Cours>();
        for (Cours C : lesCours){
            if(C.getDateCours().equals(dateCours)){
                lesCoursTrouves.add(C);
            }
        }
        return lesCoursTrouves;
    }

    @Override
    public String toString() {
        StringBuilder informations = new StringBuilder();
        informations.append(saison).append("\n").append(listeDesCours());
        return informations.toString();
    }
}
